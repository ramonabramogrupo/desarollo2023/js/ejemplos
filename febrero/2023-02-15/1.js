// creamos una constante
// las constantes no pueden variar en el programa
const NOMBRE = "Ramon Abramo";

// creamos un variable
// para crear una variable utilizamos let
// en la variable coloco un div
let caja = document.querySelector("div");

// en la instruccion anterior he utilizado un selector
// querySelector("div") selector por etiqueta
// querySelector(".nom") selector por clase
// querySelector("#identificador1") selector por id


// mostrar en el body el NOMBRE

document.write(NOMBRE);

// mostrar el nombre en la consola

console.log(NOMBRE);

// mostrar el nombre en el div

// sin utilizar la variable caja
// document.querySelector("div").innerHTML=NOMBRE
caja.innerHTML = NOMBRE;


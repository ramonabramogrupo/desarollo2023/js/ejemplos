function sumar() {
    // variables
    let numero1 = 0;
    let numero2 = 0;
    let resultado = 0;

    // entradas
    numero1 = +document.querySelector("#numero1").value;
    numero2 = +document.querySelector('#numero2').value;

    // procesamiento
    resultado = numero1 + numero2;

    // salida

    // a consola
    console.log(resultado);

    // al cuerpo de la web
    // document.write(resultado); // me borra la pantalla y solo veo la suma

    // a la caja de texto coloco el resultado
    document.querySelector('#resultado').value = resultado;
    // a la etiqueta (label) le coloco el texto suma
    document.querySelector('#lresultado').innerHTML = "Suma";

}

function restar() {
    // variables
    let numero1 = 0;
    let numero2 = 0;
    let resultado = 0;

    // entradas
    numero1 = +document.querySelector("#numero1").value;
    numero2 = +document.querySelector('#numero2').value;

    // procesamiento
    resultado = numero1 - numero2;

    // salida

    // a consola
    console.log(resultado);

    // al cuerpo de la web
    // document.write(resultado); // me borra la pantalla y solo veo la suma

    // a la caja de texto coloco el resultado
    document.querySelector('#resultado').value = resultado;
    // a la etiqueta (label) le coloco el texto suma
    document.querySelector('#lresultado').innerHTML = "Resta";

}

function multiplicar() {
    // variables
    let numero1 = 0;
    let numero2 = 0;
    let resultado = 0;

    // entradas
    numero1 = +document.querySelector("#numero1").value;
    numero2 = +document.querySelector('#numero2').value;

    // procesamiento
    resultado = numero1 * numero2;

    // salida

    // a consola
    console.log(resultado);

    // al cuerpo de la web
    // document.write(resultado); // me borra la pantalla y solo veo la suma

    // a la caja de texto coloco el resultado
    document.querySelector('#resultado').value = resultado;
    // a la etiqueta (label) le coloco el texto suma
    document.querySelector('#lresultado').innerHTML = "Producto";

}

function dividir() {
    // variables
    let numero1 = 0;
    let numero2 = 0;
    let resultado = 0;

    // entradas
    numero1 = +document.querySelector("#numero1").value;
    numero2 = +document.querySelector('#numero2').value;

    // procesamiento
    resultado = numero1 / numero2;

    // salida

    // a consola
    console.log(resultado);

    // al cuerpo de la web
    // document.write(resultado); // me borra la pantalla y solo veo la suma

    // a la caja de texto coloco el resultado
    document.querySelector('#resultado').value = resultado;
    // a la etiqueta (label) le coloco el texto suma
    document.querySelector('#lresultado').innerHTML = "Cociente";

}
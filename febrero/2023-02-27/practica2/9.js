// definir variables
let n1 = 0;
let n2 = 0;
let suma = 0,
    resta = 0,
    producto = 0,
    division = 0;

// entradas
n1 = parseInt(prompt("numero 1"));
n2 = parseInt(prompt("numero 2"));

// procesamiento
suma = n1 + n2;
resta = n1 - n2;
producto = n1 * n2;
division = n1 / n2;

// salidas

// concatenando utilizando +
console.log('La suma es' + suma);
document.write('La suma es' + suma + "<br>");


// concantenando dentro de log y write con ,
console.log('La resta es', resta);
document.write('La resta es', resta, "<br>");

// utilizando para concatenar ``
console.log(`El producto es ${producto}`);
document.write(`El producto es ${producto}<br>`);

console.log('La division es' + division);
document.write('La division es' + division + '<br>');
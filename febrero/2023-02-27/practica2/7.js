/*
	Creo las variables y las inicializo
*/
let numero1 = 0;
let numero2 = 0;
let resultado = 0;

/**
Introduzco los numeros en las variables
**/

// prompt es un metodo que retorna String
/*
	Sin utilizar parseInt
*/
// numero1 = prompt("Introduce el primer numero");
// numero2 = prompt("Introduce el primer numero");
// como los numeros los trata como texto los concatena el operador +
// resultado = numero1 + numero2; 

/**
 * Utilizando parseInt o parseFloat
 */
numero1 = parseInt(prompt("Introduce el primer numero"));

numero2 = parseInt(prompt("Introduce el primer numero"));

resultado = numero1 + numero2;

/**
Muestro el resultado en la consola
**/
console.log(resultado);

/**
 * Muestro el resultado en el body
 */
document.write(resultado);

/**
 * Muestro el resultado en un mensaje emergente
 */
alert(resultado);



// variables
let radio = 0;
let longitud = 0;
let area = 0;
let volumen = 0;

//constantes
const PI = 3.14;

// introducir dato
radio = +prompt("introduce el radio");

// procesamiento
longitud = 2 * PI * radio;

// puedo utilizar la constante PI de js
// longitud = 2 * Math.PI * radio;

//area=Math.PI*Math.pow(radio,2);
area = Math.PI * radio ** 2;

volumen = (4 / 3) * Math.PI * Math.pow(radio, 3);

// mostrar resultados
console.log("longitud: " + longitud);
console.log("Area: " + area);
console.log("Volumen: " + volumen);

// mostrar resultados
console.log(`Longitud: ${longitud}`);
console.log(`Area: ${area}`);
console.log(`Volumen : ${volumen}`);


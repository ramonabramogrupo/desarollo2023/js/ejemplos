function unir() {
    // variables
    let numero = null;
    let texto = null;
    let resultado = null;

    // entradas
    numero = document.querySelector('#numero').value;
    texto = document.querySelector('#texto').value;

    // procesamiento
    resultado = numero + texto;

    // salidas 
    console.log(resultado);

    // saco el resultado en un div
    document.querySelector('#salida').innerHTML = resultado;

    // saco el resultado en un input
    document.querySelector('#salida1').value = resultado;

}


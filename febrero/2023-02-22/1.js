// declarar variables
let tituloPagina = null;

// declarar constantes
// selectores
// por etiqueta ==> .querySelector("etiqueta")
// por clase ==> .querySelector(".clase")
// por id ==> .querySelector("#id");
// por atributo ==> .querySelector("[atributo=valor]")

const etiquetaTitulo = document.querySelector("h1");
const etiquetaTitulo1 = document.querySelector(".titulo");
const etiquetaTitulo2 = document.querySelector("#header");
const etiquetaTitulo3 = document.querySelector("[data-titulo=cabecera]");


// utilizar getElementById ==> .getElementById("id")

const etiquetaTitulo4 = document.getElementById("header");

// introducir informacion

// leo el contenido de h1 y lo almaceno en la variable
tituloPagina = etiquetaTitulo.innerHTML;

// procesar informacion
// coloco la variable titulo de la pagina en mayusculas
tituloPagina = tituloPagina.toUpperCase();

// mostrar resultados
// muestro el titulo en mayusculas
etiquetaTitulo.innerHTML = tituloPagina;


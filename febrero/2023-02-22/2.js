// variables y constantes

let texto = null;
let numeroCaracteres = 0;


// apuntando a los dos divs utilizando .getElementById
const primerDiv = document.getElementById("caja");
const segundoDiv = document.getElementById("salida");

// apuntando a los dos divs utilizando .querySelector
// const primerDiv = document.querySelector("#caja");
// const segundoDiv = document.querySelector("#salida");

// entrada

// leo el texto del primer div y lo almaceno en la variable
texto = primerDiv.innerText;

// leo la longitud del texto y lo almaceno en la variable
numeroCaracteres = texto.length;

// procesamiento


// salida

// mostrar el numero de caracteres en el segundo div
segundoDiv.innerHTML = numeroCaracteres;



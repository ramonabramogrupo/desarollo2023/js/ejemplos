// variables y constantes

// crear una constante que apunte a cada etiqueta
// donde quiero leer y escribir
const primerLi = document.getElementById("primero");
const ultimoLi = document.getElementById("ultimo");
const caja = document.getElementById("salida");

// igual pero con querySelector
// const primerLi = document.querySelector("#primero");
// const ultimoLi = document.querySelector("#ultimo");
// const caja = document.querySelector("#salida");

// creo una variable para

// numero del primer li
let numero1 = null;

// numero del ultimo li
let numero2 = null;
// almacenar los dos numeros juntos
let todo = null;

// entrada
// colocar en cada variable su valor
numero1 = primerLi.innerHTML;
numero2 = ultimoLi.innerHTML;
// procesamiento
// concatenar las dos variables
todo = numero1 + ", " + numero2;
// todo = `${numero1}, ${numero2}`;
// todo = numero1.concat(",",numero2);

// salida
// mostrar en la caja de salida los numeros concatenados
caja.innerHTML = todo;

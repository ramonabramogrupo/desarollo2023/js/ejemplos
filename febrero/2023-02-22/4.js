// variables y constantes

// creamos una constante que apunte a todos los li por clase
const lista = document.getElementsByClassName("lista");
//const lista = document.querySelectorAll(".lista");

// creo una constante que apunta a la caja de salida
const salida = document.getElementsByClassName("salida");

// crear una variable donde voy a almacenar todos los textos juntos
let todo = null;




/**
 * Utilizando directamente los li
 */

// procesamiento

todo = lista[0].innerHTML + "," + lista[1].innerHTML + "," + lista[2].innerHTML + "," + lista[3].innerHTML + "," + lista[4].innerHTML;


/**
 * Utilizando una variable para cada texto
 */

// creo las variables

// let uno = null;
// let dos = null;
// let tres = null;
// let cuatro = null;
// let cinco = null;

// entradas
// colocar cada texto en su variable
// uno = lista[0].innerHTML;
// dos = lista[1].innerHTML;
// tres = lista[2].innerHTML;
// cuatro = lista[3].innerHTML;
// cinco = lista[4].innerHTML;

// procesamiento
// todo = uno + "," + dos + "," + tres + "," + cuatro + "," + cinco;

/**
 * Utilizando una variable de tipo array para almacenar los textos
 */

// creo una variable para los textos
// let texto = [];

// creo un array con cada texto
// texto[0] = lista[0].innerHTML;
// texto[1] = lista[1].innerHTML;
// texto[2] = lista[2].innerHTML;
// texto[3] = lista[3].innerHTML;
// texto[4] = lista[4].innerHTML;


// procesamiento

// todo = texto.join();



/**
 * esto es comun para todos los metodos
 */

// salidas
salida[0].innerHTML = todo;

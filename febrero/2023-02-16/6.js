// crear las siguientes valores con valor a 0
// precio, precioParcial, precioDescuento, precioFinal, descuento, unidades
let precio = 0;
let precioParcial = 0;
let precioDescuento = 0;
let precioFinal = 0;
let descuento = 0;
let unidades = 0;


// pedir por teclado mediante prompt el precio
precio = prompt("introduce precio");

// pedir por teclado mediante prompt el descuento
descuento = prompt("introduce el descuento del producto");

// convertir el descuento a tanto por uno
descuento = descuento / 100;

// pedir por teclado mediante prompt las unidades
unidades = prompt("numero de unidades pedidas");

// calcular el precioParcial
// precioParcial=precio*unidades

precioParcial = precio * unidades;

// calcular el precioDescuento
// precioDescuento=precioParcial*descuento

precioDescuento = precioParcial * descuento;


// calcular el precioFinal
// precioFinal=precioParcial-precioDescuento
precioFinal = precioParcial - precioDescuento;


// mostrar todas las variables en cada uno de los span
// para ello cada span tiene un id con el nombre de la variable
// utilizar por lo tanto el metodo querySelector del objeto document
// utilizar la propiedad innerHTML

document.querySelector("#idPrecio").innerHTML = precio;
document.querySelector("#idPrecioParcial").innerHTML = precioParcial;
document.querySelector("#idPrecioFinal").innerHTML = precioFinal;
document.querySelector("#idPrecioDescuento").innerHTML = precioDescuento;
document.querySelector("#idUnidades").innerHTML = unidades;
document.querySelector("#idDescuento").innerHTML = descuento;







// crear una constante con el valor de PI
const PI = 3.14;

// Math.PI esta en el propio js

// crear una variable denominada radio con el valor 0
let radio = 0;

// crear una variable denominada area con el valor 0
let area = 0;

// crear una variable denominada perimetro con el valor 0
let perimetro = 0;

// pedir el valor de radio por teclado mediante el metodo prompt
// del objeto window
radio = prompt("Introduce el radio");

// calcular el area de un circulo y almacenarlo en area
// el area se calcula como el valor de pi por el radio elevado al cuadrado
area = PI * radio ** 2;  // operador potencia
// area = PI * Math.pow(radio, 2); // metodo pow

// mostrar el area del circulo en el body
document.write(area);


// calcular el perimetro de un circulo y almacenarlo en perimetro
// el perimetro se calcula como 2 por pi por el radio
perimetro = 2 * PI * radio;

// mostrar el perimetro del circulo en el body

// document.write("<br>");
// document.write(perimetro);

document.write("<br>" + perimetro);

// utilizando las herramientas de desarrollo mostrar el valor de las variables


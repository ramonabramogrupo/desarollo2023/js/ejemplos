// crear una variable denominada lado con un valor de 10
let lado = 10;

// crear dos variables denominadas area y superficie con valor 0
let area = 0;
let superficie = 0;

// calcular el area del cuadrado y almacenarlo en area
// el area de un cuadrado se calcula como lado elevado al cuadrado
area = lado ** 2;

// calcular la superficie del cuadrado y almacenarlo en superficie
// la superficie de un cuadrado se calcula como 4 por el lado
superficie = 4 * lado;

// mostrar en cada uno de los span los valores de las variables
// para ello debeis utilizar document.querySelector("")
// utilizar el selector por id
// para el lado ==> document.querySelector("#idLado")
// para el area ==> document.querySelector("#idArea")
// para la superficie ==> document.querySelector("#idSuperficie")
// debeis utilizar la propiedad innerHTML de estos elementos para modificar su valor

document.querySelector("#idLado").innerHTML = lado;
document.querySelector("#idSuperficie").innerHTML = superficie;
document.querySelector("#idArea").innerHTML = area;



// crear una variable llamada valor y asignarle 25
let valor = 25;

// crear dos variables llamadas total y precioDescuento con valores 0
let total = 0;
let precioDescuento = 0;

// let total = 0, precioDescuento = 0;

// crear una constante llamada descuento con valor 0.1
const descuento = 0.1;

// calcular el precioDescuento como
// precioDescuento=valor*descuento
precioDescuento = valor * descuento;

// calcular el total como
// total=valor-precioDescuento
total = valor - precioDescuento;


// mostrar en la consola
// valor
// total
// precioDescuento
console.log(valor);
console.log(total);
console.log(precioDescuento);

// console.log(valor,total,precioDescuento);


// mostrar en la consola
// "el valor es " + valor
// "el total es " + total
// "el precio del Descuento es" + precioDescuento

console.log("el valor es " + valor);
console.log("el total es " + total);
console.log("el precio del descuento es " + precioDescuento);
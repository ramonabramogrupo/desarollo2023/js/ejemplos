// crear 3 variables
let nombre = null;
let edad = 0;
let altura = 0;

// pedir nombre
nombre = prompt("Introduce tu nombre");

// puedo realizar ambos pasos en uno solo
// let nombre = prompt("Introduce tu nombre");

// pedir edad
edad = prompt("Introduce tu edad");
// convertir la edad a numero entero
edad = parseInt(edad); // convierto la edad a numero aunque no es necesario

// realizar la conversion y la introduccion de 
// datos en una sola instruccion
// edad = parseInt(prompt("Introduce tu edad"));

// pedir altura
altura = prompt("Introduce tu altura");

// convierto la altura en numero
altura = parseInt(altura);

// mostrar el nombre con alert
alert(nombre);

// mostrar la edad en el cuerpo de la pagina
document.write(edad);

// mostrar la altura en la consola
console.log(altura);


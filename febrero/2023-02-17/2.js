// definir las variables
let radio = 0;
let perimetro = 0;
let area = 0;


// Pedir Radio en teclado
radio = prompt("Introduce el radio");

// calcular el perimetro

perimetro = 2 * Math.PI * radio;

// calcular el area
area = Math.PI * radio ** 2;

// area = Math.PI * Math.pow(radio, 2);

// utilizando document.write
document.write("El radio es : " + radio);
document.write("<br>");

document.write("El perimetro es: " + perimetro);
document.write("<br>");


// document.write("El area es: " + area);
// document.write("<br>");

// modifico el mostrar el area mediante tilde grave
document.write(`El area es : ${area} <br>`);
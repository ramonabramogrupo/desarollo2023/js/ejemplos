// defino dos variables con valor null
let titulo = null; // para apuntar al h1
let cajas = null; // para apuntar a los div


// almaceno en la variable el objeto h1
titulo = document.querySelector("h1");

// coloco el color de fondo distinto
titulo.style.backgroundColor = "#ccc";

// coloco un margen interno de 25
titulo.style.padding = "25px";

// asignar a la variable cajas los objetos div
cajas = document.querySelectorAll("div");

cajas[0].style.color = "red";


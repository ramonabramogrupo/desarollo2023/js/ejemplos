// variables y constantes
// globales

const boton = document.querySelector('button');


// añadir manejadores de eventos a las etiquetas html

boton.addEventListener("click", calcular);



/**
 * funcion que se ejecuta
 * ante un evento de usuario
 */
function calcular() {
    // variables y constantes
    let nombre = "";
    let apellidos = "";
    let resultado = "";

    // creo una constante
    // que apunte a cada elemento
    // html sobre el que voy a 
    // interactuar

    const inputNombre = document.querySelector('#nombre');
    const inputApellidos = document.querySelector('#apellidos');
    const divSalida = document.querySelector('#salida');

    // entradas
    nombre = inputNombre.value;
    apellidos = inputApellidos.value;

    // procesamiento

    // juntando nombre y apellidos
    resultado = `${nombre} ${apellidos}`;

    // salidas

    // escribo el nombre y los apellidos en el div salida
    divSalida.innerHTML = resultado;

}


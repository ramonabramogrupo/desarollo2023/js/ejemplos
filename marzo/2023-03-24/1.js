// variables y constantes

// constante que apunta a las celdas

const celdas = document.querySelectorAll('td');

// colocar un onclick a cada celda

// utilizando un bucle for
// for (let contador = 0; contador < celdas.length; contador++) {
//     celdas[contador].addEventListener("click", function (evento) {
//         let texto = "";

//         // celda sobre la que he realizado clic
//         const celda = evento.target;

//         // leo el texto de la celda
//         texto = celda.innerHTML;

//         // cambiando el color de la celda
//         celda.style.backgroundColor = "red";

//         // mostrando el texto de la celda en un alert
//         alert(texto);


//     });
// }


// voy a utilizar foreach (metodo) porque NodeList lo tiene

celdas.forEach(function (elemento, indice) {

    // puedo utilizar
    // elemento
    // celdas[indice]

    elemento.addEventListener("click", function (evento) {
        let texto = "";

        // celda sobre la que he realizado clic
        const celda = evento.target;

        // leo el texto de la celda
        texto = celda.innerHTML;

        // cambiando el color de la celda
        celda.style.backgroundColor = "red";

        // mostrando el texto de la celda en un alert
        alert(texto);


    });
});
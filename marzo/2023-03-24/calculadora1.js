// variables y constantes globales

// operacion que tengo que realizar
let operacionPendiente = null;

// ultimo boton que he pulsado
// si es numero seguire añadiendo a pantalla los numeros pulsados
// si es una operacion entonces borro pantalla y escribo el numero
let ultimoBoton = null;


// variable para almacenar el numero que tenia la pantalla
// anteriormente
let numeroAnterior = 0;

// constante que apunta a todos los botones de numeros
const botonesNumeros = document.querySelectorAll('.numeros');
// constante que apunta a todos los botones de operaciones
const botonesOperaciones = document.querySelectorAll('.operacion');


// asignar a los elementos de html sobre los que queremos interactuar
// un click

botonesNumeros.forEach(function (botonNumero, indice) {
    botonNumero.addEventListener("click", function (evento) {
        // creo una constante que apunte al boton pulsado
        const boton = evento.target;

        // creo una constante que apunte a la pantalla
        const pantalla = document.querySelector('#pantalla');

        // compruebo si en la pantalla tengo un 0
        // si tengo un 0 solo coloco un el numero pulsado
        if (pantalla.innerHTML == 0) {
            // escribir en la pantalla el texto del boton pulsado
            pantalla.innerHTML = boton.innerHTML;
        } else {
            // añado a la pantalla el texto del boton pulsado
            pantalla.innerHTML += boton.innerHTML;
        }

    });
});

botonesOperaciones.forEach(function (botonOperacion) {
    botonOperacion.addEventListener("click", function (evento) {
        // constante que me indica el boton pulsado de operacion
        const boton = evento.target;

        // creo una constante que apunte a la pantalla
        const pantalla = document.querySelector('#pantalla');




        if (operacionPendiente == null) {
            operacionPendiente = boton.innerHTML;
            numeroAnterior = +pantalla.innerHTML;
        } else {

        }

    });
});
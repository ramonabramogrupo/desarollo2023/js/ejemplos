/**
 * suma 3 numeros
 * @param {*} numero1 
 * @param {*} numero2 
 * @param {*} numero3 
 * @returns la suma de los 3 numeros
 */
function uno(numero1, numero2 = 0, numero3 = 0) {
    return numero1 + numero2 + numero3;
}

/**
 * multiplica dos numeros
 * @param {*} numero1 
 * @param {*} numero2 
 * @returns el producto de los dos numeros
 */

function producto(numero1, numero2) {
    // variables y constantes
    let resultado = 0;

    // procesamiento

    resultado = numero1 * numero2;

    // salida
    return resultado;
}

/**
 * Sumar todos los numeros del array
 * @param {array} numeros 
 * @returns suma del array
 */
function sumaArray(numeros) {
    let resultado = 0;

    numeros.forEach(function (valor, indice) {
        resultado = resultado + valor;
    });

    return resultado;
}

/**
 * sumar los numeros positivos del array
 * @param {array} numeros 
 * @returns suma de los numeros positivos del array
 */

function sumaPositivosArray(numeros) {
    let salida = 0;

    numeros.forEach(function (valor) {
        if (valor > 0) {
            salida = salida + valor;
        }

    });

    return salida;
}

/**
 * Recibe un array y suma los numeros pares
 * @param {array} numeros 
 * @returns suma de los pares
 */
function sumaParesArray(numeros) {
    let resultado = 0;

    numeros.forEach(function (valor) {

        if (valor % 2 == 0) {
            resultado += valor;
            // resultado = resultado + valor;
        }
    });

    return resultado;
}

/**
 * Recibe un array y suma los numeros pares
 * @param {array} numeros 
 * @returns suma de los pares
 */
function sumaParesArray1(numeros) {
    let resultado = 0;

    numeros.forEach(function (valor) {

        if (esPar(valor)) {
            resultado += valor;
            // resultado = resultado + valor;
        }
    });

    return resultado;
}

/**
 * recibe un numero y comprueba si es par
 * @param {*} numero 
 * @returns true si es par o false si es impar 
 */
function esPar(numero) {
    let salida = 0;
    if (numero % 2 == 0) {
        salida = true;
    } else {
        salida = false;
    }
    return salida;
}



/**
 * Cuenta el numero de 'a'
 * @param {string} texto 
 * @returns la cantida de letras a que tiene el texto
 */
function contarA(texto) {
    let salida = 0;
    let vector = [];

    // convierto el string en un array
    vector = texto.split("");

    vector.forEach(function (valor) {
        if (valor == 'a' || valor == 'A') {
            salida++;
        }
    });

    return salida;

}


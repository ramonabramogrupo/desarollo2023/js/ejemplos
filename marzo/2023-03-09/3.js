/**
 * Recibe un array y suma los numeros pares
 * @param {array} numeros 
 * @returns suma de los pares
 */
function sumaParesArray(numeros) {
    let resultado = 0;

    numeros.forEach(function (valor) {

        if (valor % 2 == 0) {
            resultado += +valor;
            // resultado = resultado + +valor;
        }
    });

    return resultado;
}

/**
 * Sumar todos los numeros del array
 * @param {array} numeros 
 * @returns suma del array
 */
function sumaArray(numeros) {
    let resultado = 0;

    numeros.forEach(function (valor, indice) {
        resultado = resultado + +valor;
    });

    return resultado;
}

/**
 * sumar los numeros positivos del array
 * @param {array} numeros 
 * @returns suma de los numeros positivos del array
 */

function sumaPositivosArray(numeros) {
    let salida = 0;

    numeros.forEach(function (valor) {
        if (valor > 0) {
            salida = salida + +valor;
        }

    });

    return salida;
}

// -----------------------------------------------------------
/**
 * funciones que escuchan el click en los botones
 */

// --------------------------------

function sumaPares() {
    // variables y constantes

    let resultado = 0;
    let numerosTexto = "";
    let numerosArray = [];

    // creo dos constantes que apunten a las
    // caja de entrada
    // div de salida
    const inputNumeros = document.querySelector('#numeros');
    const divResultado = document.querySelector('#resultado');

    // entradas

    // leo los numeros del input
    numerosTexto = inputNumeros.value;

    // convierto los numeros (string) en array
    numerosArray = numerosTexto.split(",");

    // procesamiento

    resultado = sumaParesArray(numerosArray);

    // salidas

    divResultado.innerHTML = resultado;

}

function sumaPositivos() {
    // variables y constantes

    let resultado = 0;
    let numerosTexto = "";
    let numerosArray = [];

    // creo dos constantes que apunten a las
    // caja de entrada
    // div de salida
    const inputNumeros = document.querySelector('#numeros');
    const divResultado = document.querySelector('#resultado');

    // entradas

    // leo los numeros del input
    numerosTexto = inputNumeros.value;

    // convierto los numeros (string) en array
    numerosArray = numerosTexto.split(",");

    // procesamiento

    resultado = sumaPositivosArray(numerosArray);

    // salidas

    divResultado.innerHTML = resultado;
}

function suma() {
    // variables y constantes

    let resultado = 0;
    let numerosTexto = "";
    let numerosArray = [];

    // creo dos constantes que apunten a las
    // caja de entrada
    // div de salida
    const inputNumeros = document.querySelector('#numeros');
    const divResultado = document.querySelector('#resultado');

    // entradas

    // leo los numeros del input
    numerosTexto = inputNumeros.value;

    // convierto los numeros (string) en array
    numerosArray = numerosTexto.split(",");

    // procesamiento

    resultado = sumaArray(numerosArray);

    // salidas

    divResultado.innerHTML = resultado;
}
function calcular() {
    // variables y constantes
    let nombre = null;
    let apellidos = null;
    let resultado = [];

    // constante que apunte a nombre
    const inputNombre = document.querySelector('#nombre');

    // constante que apunta a los apellidos
    const inputApellidos = document.querySelector('#apellidos');

    // constante que apunte al div de nombre completo
    const divNombreCompleto = document.querySelector('#nombreCompleto');

    // constante que apunte al div de iniciales
    const divIniciales = document.querySelector('#iniciales');

    // entradas
    nombre = inputNombre.value;
    apellidos = inputApellidos.value;

    // procesamiento

    resultado[0] = concatenar([nombre, apellidos]);
    resultado[1] = concatenar([nombre[0], ".", apellidos[0], "."])

    // salidas
    divNombreCompleto.innerHTML = resultado[0];
    divIniciales.innerHTML = resultado[1];

}

/**
 * funcion que concatena todos los elementos
 * del array pasado como argumento
 * @param {array} textos array con textos
 * @returns texto con todos los elementos del array juntos
 */
function concatenar(textos) {
    let salida = "";

    textos.forEach(function (valor) {
        salida = salida + " " + valor;
    });

    return salida;
}


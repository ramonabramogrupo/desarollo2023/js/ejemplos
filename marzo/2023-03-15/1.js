function enviar() {
    // variables y constantes
    let resultado = "";
    let direccion = "";
    let poblacion = "";
    let provincia = ""

    // creo una constante que apunte a cada elemento
    // de la web sobre el que a interactuar

    const inputDireccion = document.querySelector('#dir');
    const inputPoblacion = document.querySelector('#pob');
    const inputProvincia = document.querySelector('#pro');
    const cajaResultado = document.querySelector('#result');


    // entradas
    direccion = inputDireccion.value;
    poblacion = inputPoblacion.value;
    provincia = inputProvincia.value;

    // procesamiento
    resultado = "Los datos son correctos:<br>";
    resultado += `${direccion}<br>${poblacion}<br>${provincia}<br>`;
    // resultado += direccion + "<br>" + poblacion + "<br>" + provincia + "<br>";


    // salidas
    cajaResultado.innerHTML = resultado;

}



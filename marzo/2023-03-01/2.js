// variables

let numeros = [];
// let numeros = new Array();

// entrada

numeros[0] = +prompt("introduce numero");
numeros[1] = +prompt("introduce numero");
numeros[2] = +prompt("introduce numero");
numeros[3] = +prompt("introduce numero");

// salida

console.log(numeros);

document.write(numeros);
document.write("<br>");

document.write(numeros.join("-"));
document.write("<br>");

// ordena alfabeticamente
document.write(numeros.sort());
document.write("<br>");

// ordena numericamente
document.write(numeros.sort(function (a, b) { return a - b }));
document.write("<br>");


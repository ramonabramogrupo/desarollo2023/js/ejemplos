// variables y constantes

let texto = null;
let caracter = null;

const divSalida = document.querySelector('#salida');

// entradas

texto = prompt("Introduce una cadena de caracteres");

// procesamiento

// estoy leyendo la 4 posicion del string
// utilizo la nomemclatura de arrays
// caracter = texto[3];

// utilizando el metodo charAt
caracter = texto.charAt(3);


// salida

// lo escribo en la consola
console.log(caracter);

// lo escribo en el div
divSalida.innerHTML = caracter;







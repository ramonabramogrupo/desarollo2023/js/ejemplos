// variables y constantes
let nota = 0;
let salida = null;

const baremos = [
    "muy deficiente",
    "suspenso",
    "aprobado",
    "bien",
    "notable",
    "sobresaliente"
];

const soluciones = new Array(11);

// introducir

nota = parseInt(prompt("Introduce nota"));

// nota = 2 ==> 1
// nota = 4 ==> 1

// 0 - 1 ==> 0
// 1 - 5 ==> 1
// 5 - 6 ==> 2
// 6 - 7 ==> 3
// 7 - 9 ==> 4
// 9 - 10 => 5

// procesamiento

// vamos a utilizar un array

// esto es para el muy deficiente
soluciones[0] = baremos[0];

// este es para el suspenso
// soluciones[1] = baremos[1];
// soluciones[2] = baremos[1];
// soluciones[3] = baremos[1];
// soluciones[4] = baremos[1];
// puedo utilizar el metodo fill

soluciones.fill(baremos[1], 1, 5);

// este es para el aprobado
soluciones[5] = baremos[2];

// este es para el bien
soluciones[6] = baremos[3];

// este es para el notable
// soluciones[7] = baremos[4];
// soluciones[8] = baremos[4];
soluciones.fill(baremos[4], 7, 9);


// este es para el sobresaliente
// soluciones[9] = baremos[5];
// soluciones[10] = baremos[5];
soluciones.fill(baremos[5], 9, 11);


// la nota se utiliza como indice en else{
// array de soluciones

salida = soluciones[nota];

// salida

console.log(salida);
document.write(salida);

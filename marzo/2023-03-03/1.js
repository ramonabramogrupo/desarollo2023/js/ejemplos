// variables y constantes
let nota = 0;
let salida = null;

// introducir

nota = +prompt("Introduce nota");

// procesamiento

// vamos a utilizar un if elseif

if (nota < 1) {
    salida = "muy deficiente";
} else if (nota < 5) {
    salida = "suspenso";
} else if (nota < 6) {
    salida = "aprobado";
} else if (nota < 7) {
    salida = "bien";
} else if (nota < 9) {
    salida = "notable";
} else if (nota <= 10) {
    salida = "sobresaliente";
} else {
    salida = "nota incorrecta";
}

// salida

console.log(salida);
document.write(salida);

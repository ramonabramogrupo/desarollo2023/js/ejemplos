// variables y contantes

let numero = 0;
let salida = null;

const meses = [
    "enero",
    "febrero",
    "marzo",
    "abril",
    "mayo",
    "junio",
    "julio",
    "agosto",
    "septiembre",
    "octubre",
    "noviembre",
    "diciembre"
];

// entradas

numero = +prompt("introduce mes en numero");

// procesamiento

// utilizando arrays
salida = meses[numero - 1];

// salida

console.log(salida);
document.write(salida);


// variables y constantes
let nota = 0;
let salida = null;

// introducir

nota = +prompt("Introduce nota");

// procesamiento

// vamos a utilizar un switch

switch (true) {
    case nota < 1:
        salida = "muy deficiente";
        break;
    case nota < 5:
        salida = "suspenso";
        break;
    case nota < 6:
        salida = "aprobado";
        break;
    case nota < 7:
        salida = "bien";
        break;
    case nota < 9:
        salida = "notable";
        break;
    case nota <= 10:
        salida = "sobresaliente";
        break;
    default:
        salida = "incorrecto";
        break;
}

// salida

console.log(salida);
document.write(salida);

// variables y constantes globales

// variable para controlar el paso (desplazamiento)
let paso = 5;

// variable controla la direccion de la pieza
let direccion = "derecha";

// variable para controlar el timing (setInterval)
let temporizador = null;

// puntuacion del juego
let puntuacion = 0;

// creo un array como constante
// para que no cambie el tipo de dato
// pero puedo modificar su contenido
const posiciones = [
    200, // posicion inicial de la pieza en x (left)
    200, // posicion inicial de la pieza en y (top)
];

// constante con las dimensiones del lienzo
const dimensiones = [
    500, // ancho
    500 // alto
];

// constante que apunte a la pieza
const pieza = document.querySelector('#pieza');


// funcion acabar juego
function gameOver() {
    // parando el temporarizado de moviemiento
    clearInterval(temporizador);

    // mensaje
    alert("GAME OVER");

    // preguntamos si quiere volver a jugar
    if (confirm("¿Quieres probar otra vez?")) {
        location.reload(); // recargo la pagina
    }
}


// procesamiento

// salida
pieza.style.left = posiciones[0] + "px";
pieza.style.top = posiciones[1] + "px";

// gestion de eventos

// controlar la pulsacion de los cursores
document.querySelector('body').addEventListener("keydown", function (evento) {
    // variables y constantes locales
    let teclaPulsada = null;

    // entradas
    teclaPulsada = evento.key;

    // procesamiento
    switch (teclaPulsada) {
        case 'ArrowLeft':
            direccion = "izquierda";
            break;
        case 'ArrowRight':
            direccion = "derecha";
            break;
        case 'ArrowUp':
            direccion = "arriba";
            break;
        case 'ArrowDown':
            direccion = "abajo";
            break;
    }

    // salidas


});


// control de tiempos
// control del evento setInterval

// desplaza la pieza cada x tiempo
temporizador = window.setInterval(function () {
    // variables y constantes locales

    // constante que apunte al marcador
    const marcador = document.querySelector('#marcador span');

    // funciones locales
    function controlColisiones() {
        // controlo si estoy fuera del lienzo
        if (posiciones[0] >= dimensiones[0]) {
            // choca con la parte derecha
            gameOver();
        } else if (posiciones[1] >= dimensiones[1]) {
            // choca con la parte inferior
            gameOver();
        } else if (posiciones[0] <= 0) {
            // choca con la parte izquierda
            gameOver();
        } else if (posiciones[1] <= 0) {
            // choca con la parte superior
            gameOver();
        }
    }
    // entradas

    // procesamiento
    switch (direccion) {
        case 'izquierda':
            posiciones[0] -= paso;
            break;
        case 'derecha':
            posiciones[0] += paso;
            break;
        case 'arriba':
            posiciones[1] -= paso;
            break;
        case 'abajo':
            posiciones[1] += paso;
            break;
    }

    controlColisiones();
    // incremento un punto si no se ha chocado
    puntuacion++;

    //salidas

    // colocar la pieza en la coordenada actual
    pieza.style.left = posiciones[0] + "px";
    pieza.style.top = posiciones[1] + "px";

    // colocar la puntuacion en el marcador
    marcador.innerHTML = puntuacion;

}, 100);


// controlar la velocidad
// cada 5 segundos va a ir mas rapido
window.setInterval(function () {
    // incremento el paso
    paso++;
}, 5000);



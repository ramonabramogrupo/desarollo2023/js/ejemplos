// funciones
function terminar(texto) {
    let salida = null;

    // texto[texto.length-1]== 's'
    // texto.slice(-1)=='s'
    // texto.endsWith('s')

    // colocar el texto en minusculas
    texto = texto.toLowerCase();

    if (texto.endsWith('s') || texto.endsWith('m')) {
        salida = "Si";
    } else {
        salida = "No";
    }

    return salida;
}

// // variables y constantes
// let dato = null;
// let final = null;

// // entradas
// dato = prompt("introduce tu nombre");

// // procesamiento
// final = terminar(dato);

// //salidas
// alert(final);

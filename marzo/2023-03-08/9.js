// funciones

/**
 * funcion para calcular numero de caracteres
 * @param {*} entrada texto
 * @returns numero de caracteres del texto
 */
function longitud(entrada) {
    return entrada.length;
}

// variables y constantes
let texto = null;
let salida = 0;

// entradas
texto = prompt("introduce un texto");

// procesamiento

// voy a pasar a la funcion argumentos
longitud(texto, salida);


// salidas
alert(salida);
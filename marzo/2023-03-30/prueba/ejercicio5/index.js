// variable y constantes globales

// constantes que apunten a los elementos de html
// que quiero controlar

const boton = document.querySelector('#enviar');


// control de eventos
boton.addEventListener("click", function (evento) {
    // variables y constantes locales
    let salida = "";
    let textoCorreo1 = "";
    let textoCorreo2 = "";

    // constante que apunte a cada elemento
    // que voy a utilizar
    const correo1 = document.querySelector('#correo');
    const correo2 = document.querySelector('#correoConfirmacion');
    const div = document.querySelector('#caja');

    //entrada
    textoCorreo1 = correo1.value;
    textoCorreo2 = correo2.value;

    // procesamiento

    // coloco los correos en minusculas
    textoCorreo1 = textoCorreo1.toLowerCase();
    textoCorreo2 = textoCorreo2.toLowerCase();

    if (textoCorreo1 == textoCorreo2) {
        salida = "Correcto";
    } else {
        salida = "Incorrecto";
    }

    // salida
    div.innerHTML = salida;

});
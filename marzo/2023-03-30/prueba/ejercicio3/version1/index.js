// variables y constantes
let numeros = [];

let promedio = 0;
let acumulador = 0;

// coloco el numero de valores a pedir
const VALORES = 10;

// entradas
for (let contador = 0; contador < VALORES; contador++) {
    numeros[contador] = +prompt("Introduce un numero");
}

// para probar
// numeros = [3, 3, 5, 6, 12, 3, 4, 5, 6, 10];

// procesamiento
numeros.forEach(function (numero) {
    acumulador += numero;
});

promedio = acumulador / VALORES;

// salida

document.write(`Promedio:${promedio}`);
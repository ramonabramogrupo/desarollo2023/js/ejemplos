// variables y constantes globales

// constante que apunte a los elementos sobre
// los que voy a interactuar
const boton = document.querySelector('#convertir');

// funciones 
function calcula(bytes) {
    // constantes y variables locales
    let resultado = [];

    // procesamiento
    resultado[0] = bytes / 1024;
    resultado[1] = bytes / 1024 ** 2;
    resultado[2] = bytes / 1024 ** 3;

    return resultado;
}

// gestion de eventos
boton.addEventListener("click", function (evento) {
    // variables y constantes locales
    let valorBytes = 0;
    let resultado = [];

    // constantes que apunten a los elementos de html
    // sobre los que voy a trabajar
    const inputBytes = document.querySelector('#bytes');
    const div = document.querySelector('#caja');

    // entrada
    valorBytes = inputBytes.value;

    // procesamiento
    if (valorBytes >= 0 && valorBytes <= 2000000000) {
        resultado = calcula(valorBytes);
    } else {
        resultado = "mal";
    }


    // salidas
    if (resultado == "mal") {
        div.innerHTML = "El numero esta fuera de rango";
    } else {
        div.innerHTML = `KB: ${resultado[0]}<br>`;
        div.innerHTML += `MB: ${resultado[1]}<br>`;
        div.innerHTML += `GB: ${resultado[2]}<br>`;
    }
});

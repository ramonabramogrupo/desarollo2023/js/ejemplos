// variables y constantes globales

// constante que apunta al boton
const boton = document.querySelector("#enviar");

// colocar clic al boton enviar
boton.addEventListener("click", function (evento) {
    // variables y constantes locales
    let tamano = 0;
    let grosor = 0;

    // crear una constante que apunte
    // a cada elemento sobre el que teneis
    // interaccionar
    const inputTamano = document.querySelector('#tamano');
    const inputGrosor = document.querySelector('#grosor');
    const div = document.querySelector('#caja');

    // entradas
    tamano = inputTamano.value;
    grosor = inputGrosor.value;

    // procesamiento

    // salidas
    div.style.width = tamano + "px";
    div.style.borderWidth = grosor + "px";
});

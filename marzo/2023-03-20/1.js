// funcion que se llama cuando
// hago clic en cualquiera de los botones
function color(nombreColor) {
    // variables y constantes

    // creo un element
    // apunto a la caja que quiero cambiar el color
    const caja = document.querySelector('div');

    // procesamiento

    caja.style.backgroundColor = nombreColor;

}
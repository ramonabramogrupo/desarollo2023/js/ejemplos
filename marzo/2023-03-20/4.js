// constante que apunte a todos los td

const celdas = document.querySelectorAll('td');

// colocar un manejador de eventos a cada celda

celdas[0].addEventListener("click", function (evento) {

    // esto es el elemento sobre el que pulsaste
    const elemento = evento.target;

    // cambio el color de fondo de la celda
    elemento.style.backgroundColor = "black";


});

celdas[1].addEventListener("click", function (evento) {

    // esto es el elemento sobre el que pulsaste
    const elemento = evento.target;

    // cambio el color de fondo de la celda
    elemento.style.backgroundColor = "black";


});

/**
 *  mi funcion anonima evita utilizar funciones con nombre
 *
 */
// function cambiaColor(elemento) {
//     elemento.style.backgroundColor = "black";
// }
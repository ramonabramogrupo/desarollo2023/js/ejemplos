// variables y constantes

// necesito una constante que apunte a cada elemento sobre el que el
// usuario puede interactuar

const botones = document.querySelectorAll('button');

// colocar un onclick a cada boton
// utilizo el addEventListener para colocar el manejador de eventos


/** 
 * coloco 
 * los colores a mano
 * 
*/

// botones[0].addEventListener("click", function (evento) {
//     // apunto al div que quiero cambiar el color
//     const caja = document.querySelector('div');

//     // coloco el div de ese colo
//     caja.style.backgroundColor = "red";
// });

// botones[1].addEventListener("click", function (evento) {
//     // apunto al div que quiero cambiar el color
//     const caja = document.querySelector('div');

//     // coloco el div de ese colo
//     caja.style.backgroundColor = "green";
// });

// botones[1].addEventListener("click", function (evento) {
//     // apunto al div que quiero cambiar el color
//     const caja = document.querySelector('div');

//     // coloco el div de ese colo
//     caja.style.backgroundColor = "blue";
// });


/**
 * Coloco los colores usando atributos data 
 */

botones[0].addEventListener("click", function (evento) {
    // apunto al div que quiero cambiar el color
    const caja = document.querySelector('div');

    // boton que he pulsado
    const boton = evento.target;

    // leo el color del boton pulsado
    const nombreColor = boton.getAttribute("data-color");

    // coloco el div de ese colo
    caja.style.backgroundColor = nombreColor;
});

botones[1].addEventListener("click", function (evento) {
    // apunto al div que quiero cambiar el color
    const caja = document.querySelector('div');

    // boton que he pulsado
    const boton = evento.target;

    // leo el color del boton pulsado
    const nombreColor = boton.getAttribute("data-color");

    // coloco el div de ese colo
    caja.style.backgroundColor = nombreColor;
});
botones[2].addEventListener("click", function (evento) {
    // apunto al div que quiero cambiar el color
    const caja = document.querySelector('div');

    // boton que he pulsado
    const boton = evento.target;

    // leo el color del boton pulsado
    const nombreColor = boton.getAttribute("data-color");

    // coloco el div de ese colo
    caja.style.backgroundColor = nombreColor;
});


/**
 * lo podemos realizar con un bucle
 */

// for (c = 0; c < 4; c++) {
//     botones[c].addEventListener("click", function (evento) {
//         // apunto al div que quiero cambiar el color
//         const caja = document.querySelector('div');

//         // boton que he pulsado
//         const boton = evento.target;

//         // leo el color del boton pulsado
//         const nombreColor = boton.getAttribute("data-color");

//         // coloco el div de ese colo
//         caja.style.backgroundColor = nombreColor;
//     })
// }

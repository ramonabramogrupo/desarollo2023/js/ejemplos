// variables y constantes

// necesito una constante que apunte a cada elemento sobre el que el
// usuario puede interactuar

const botones = document.querySelectorAll('button');

// colocar un onclick a cada boton
// utilizo el addEventListener para colocar el manejador de eventos

botones[0].addEventListener("click", function () {
    color("red");
});

botones[1].addEventListener("click", function () {
    color("green");
});

botones[2].addEventListener("click", function () {
    color("blue");
});

// funcion que se llama cuando
// hago clic en cualquiera de los botones
function color(nombreColor) {
    // variables y constantes

    // creo un element
    // apunto a la caja que quiero cambiar el color
    const caja = document.querySelector('div');

    // procesamiento

    caja.style.backgroundColor = nombreColor;

}




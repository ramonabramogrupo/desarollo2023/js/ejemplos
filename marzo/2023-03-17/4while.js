// INTRODUCE UN NUMERO POR TECLADO
// MOSTRAR EN LA PANTALLA CON DOCUMENT.WRITE
// TODOS LOS NUMEROS PARES DESDE 0 HASTA EL
// NUMERO INTRODUCIDO POR TECLADO

// variables y constantes

let numero = 0;
let contador = 0;
let salida = "";

// entrada
numero = +prompt("introduce un numero");

// procesamiento

// con while
contador = 0;
while (contador <= numero) {
    salida += `${contador} `;
    contador += 2;
}

// salida

document.write(salida);




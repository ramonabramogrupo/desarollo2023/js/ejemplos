// pedir un numero teclado
// el numero debe ser menor que 10
// mostrar como imagenes los numeros desde 0 hasta el numero introducido

// variables y constantes
let numero = 0;
let contador = 0;

const fotos = [
    '0.jpg',
    '1.jpg',
    '2.jpg',
    '3.jpg',
    '4.jpg',
    '5.jpg',
    '6.jpg',
    '7.jpg',
    '8.jpg',
    '9.jpg'
];


// entradas
numero = +prompt("introduce un numero menor que 10");

// procesamiento
for (contador = 1; contador <= numero; contador++) {
    document.write(`<img src="imgs/${fotos[contador]}">`);
}

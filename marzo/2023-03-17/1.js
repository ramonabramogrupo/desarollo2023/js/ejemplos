// variables

let texto = "";
let contador = 0;
let acumulador = 0;

// entradas

texto = "Ejemplo de clase";

// procesamiento

// contar numero de letras 'e'
for (contador = 0, acumulador = 0; contador < texto.length; contador++) {
    // leo cada caracter
    let letra = texto[contador];

    // compruebo si el caracter es una e
    if (letra.toLowerCase() == 'e') {
        acumulador++;
    }
}

// salida
// mostrando el numero de letras 'e'
alert(acumulador);



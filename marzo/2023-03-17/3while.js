// imprimir una torre de numeros
// maximo=4
// 1
// 12
// 123
// 1234

// variables y constantes
let acumulador = "";
let contador = 0;
let salida = "";

const MAXIMO = 4;

// procesamiento

// solucion con un while

contador = 1;
while (contador <= MAXIMO) {
    acumulador += contador;
    salida += acumulador + "<br>";
    contador++;
}


// salidas
document.write(salida);


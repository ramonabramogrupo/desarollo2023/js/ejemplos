// imprimir una torre de numeros
// maximo=4
// 1
// 12
// 123
// 1234

// variables y constantes
let acumulador = "";
let contador = 0;
let salida = "";

const MAXIMO = 4;

// procesamiento

// solucion con un for

for (let contador = 1; contador <= MAXIMO; contador++) {
    acumulador += contador;
    salida += acumulador + "<br>";
}

// salidas
document.write(salida);


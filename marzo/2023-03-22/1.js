// variables y constantes globales

// funciones

// entradas

// procesamiento


// salidas

// funciones que se ejecuten cuando interactuo sobre los elementos de html

/**
 * funcion que se ejecute al pulsar el boton
 */
function cambiar() {
    // variables y constantes

    // el div que quiero cambiar
    const caja = document.querySelector('div');

    // procesamiento
    caja.style.width = "300px";
    caja.style.height = "300px";
    // salidas

}
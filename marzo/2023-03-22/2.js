// variables y constantes globales

//  creo una constante que apunta al boton
const boton = document.querySelector('button');

// colocar los manejadores de eventos
// que quiero que tengan cada uno de los elementos
// de la pagina

// colocar el onlick al boton

boton.addEventListener("click", function (evento) {
    // variables y constantes

    // constante que apunta a la caja 
    // que quiero modificar
    const caja = document.querySelector('div');

    caja.style.width = "300px";
    caja.style.height = "300px";
});


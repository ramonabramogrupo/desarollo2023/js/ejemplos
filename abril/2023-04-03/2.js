// objeto de js
// creando un objeto instanciado
let posiciones = new Object();

// modificando el valor de la propiedad x
posiciones['x'] = 10; // utilizando nomenclatura de arrays
posiciones.x = 10; // utilizando la nomemclatura de objetos

// leer la propiedad y
console.log(posiciones['y']); // utilizando la nomemclatura de arrays
console.log(posiciones.y); // utilizando la nomenclatura de objetos

// añadir una propiedad nueva al objecto
posiciones['z'] = 0;
posiciones.z = 0;

// ver todo el objeto (numerable)
console.log(posiciones);





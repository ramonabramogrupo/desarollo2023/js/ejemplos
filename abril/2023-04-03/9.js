let pieza = {
    // propiedades del objecto
    // variables creadas dentro del objeto
    x: 0,
    y: 0,
    ancho: 100,
    alto: 100,
    color: "#ccc",

    // metodos del objeto
    // funciones asignadas dentro del objeto
    dibujar: function (destino) {
        // creo objeto de tipo div
        let salida = document.createElement("div");
        // colocar los estilos al div
        salida.style.width = this.ancho + "px";
        salida.style.height = this.alto + "px";
        salida.style.backgroundColor = this.color;
        // mostrando el div en el body
        document.querySelector(destino).appendChild(salida);

    },
    // metodo que se va a ejecutar automaticamente 
    // cuando intente tratar el objeto como un texto
    toString: function () {
        let salida = "";
        salida = `${this.ancho},${this.alto},${this.x},${this.y},${this.color}`;
        return salida;
    }
};

pieza.dibujar("body");

// cambio las propiedades del objeto
pieza.ancho = 200;
pieza.alto = 500;
//vuelvo a dibujar
pieza.dibujar("body");

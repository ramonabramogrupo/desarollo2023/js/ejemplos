// datos primitivos

let a = 10;
let b = "hola";

// objetos

// objetos de tipo array
const numeros = [12, 23, 45, 6];

// manejar los objetos del documento (DOM)
const div = document.querySelector('div');

// crear mi propio element de html
const divCreado = document.createElement("div");


// objetos propios (literal)
const comecocos = {
    // propiedades
    posicionX: 0,
    posicionY: 0,
    //metodos
    comer: function () {
        alert("come");
    },
    toString: function () {
        return "soy un comecocos";
    }

};

// salida

document.write(a);

// añado el div creado al body
document.querySelector("body").appendChild(divCreado);
// objeto de js
// creando un objeto de forma literal
let posiciones = {
    x: 0,
    y: 0,
};

// creando un objeto de forma literal sin propiedades
let posicionesMalo = {

};

// modificando el valor de la propiedad x
posiciones['x'] = 10; // utilizando nomenclatura de arrays
posiciones.x = 10; // utilizando la nomemclatura de objetos

// leer la propiedad y
console.log(posiciones['y']); // utilizando la nomemclatura de arrays
console.log(posiciones.y); // utilizando la nomenclatura de objetos

// añadir una propiedad nueva al objecto
posiciones['z'] = 0;
posiciones.z = 0;

// al asignar un objeto se realiza por referencia
posicionesMalo = posiciones;

// al asignarse por referencia
// lo que toques en posicionesMalo se realiza tambien en posiciones
// y viceversa
posicionesMalo.z = 20;

// mostrar los dos objectos
console.log(posiciones, posicionesMalo);




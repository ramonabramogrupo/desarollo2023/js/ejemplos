const persona = {
    // propiedades
    nombre: 'Eva',
    direccion: 'mi calle',
    telefono: '987987987',

    //metodos
    hablar: function () {
        alert("hola");
    },

    dormir: function () {
        alert("adios");
    },

    presentar: function () {
        alert(`Mi nombre es ${this.nombre} y vivo en ${this.direccion}`);
    }


};

// llamo al metodo presentar
persona.presentar();

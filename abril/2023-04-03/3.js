let pieza = {
    // propiedades del objecto
    // variables creadas dentro del objeto
    x: 0,
    y: 0,
    ancho: 100,
    alto: 100,
    color: "#ccc",

    // metodos del objeto
    // funciones asignadas dentro del objeto
    dibujar: function () {
        let salida = "";
        salida = `${this.ancho},${this.alto},${this.x},${this.y},${this.color}`;
        return salida;
    },
    // metodo que se va a ejecutar automaticamente 
    // cuando intente tratar el objeto como un texto
    toString: function () {
        let salida = "";
        salida = `${this.ancho},${this.alto},${this.x},${this.y},${this.color}`;
        return salida;
    }
};

// leer el color
console.log(pieza.color);

// acceder al metodo dibujar
console.log(pieza.dibujar());
document.write(pieza.dibujar());

// al leer el objeto llama al metodo toString
console.log(pieza); // muestra el contenido del objeto

document.write(pieza); //llama al metodo toString
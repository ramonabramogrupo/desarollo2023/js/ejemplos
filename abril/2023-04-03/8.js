// añadir al final de la pagina un div con el texto final

//utilizando innerHTML
document.querySelector('body').innerHTML += '<div>Final</div>';

// utilizando createElement
// creo el elemento
let div = document.createElement("div");
// añado el texto
div.innerHTML = "final";
// añadir el elemento a la web
document.querySelector('body').appendChild(div);


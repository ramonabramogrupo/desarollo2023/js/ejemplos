// variables y constantes globales

const boton = document.querySelector('button');

// funciones

// entradas

// procesamiento

// salidas

// control de eventos
// colocar un escuchador al boton (onclick)
boton.addEventListener("click", function (evento) {
    // variables y constantes locales

    // constante apunta al lienzo
    const lienzo = document.querySelector('#lienzo');

    // constante con la etiqueta div
    const div = document.createElement("div");

    // procesamiento
    div.className = "cuadrados";

    // salida
    lienzo.appendChild(div);

});
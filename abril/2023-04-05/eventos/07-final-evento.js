/*
constantes que apuntan a los botones
*/
const botonEncender = document.querySelector('#encender');
const botonApagar = document.querySelector('#apagar');
const botonEliminar = document.querySelector('#eliminar');


// control de eventos
botonEncender.addEventListener("click", function (e) {
    document.querySelector("img").style.display = 'inline-block';
    document.querySelector("img").src = "img/on.gif";
    document.querySelector("img").alt = "bombilla encendida";
});

botonApagar.addEventListener("click", (e) => {
    document.querySelector("img").style.display = 'inline-block';
    document.querySelector("img").src = "img/off.gif";
    document.querySelector("img").alt = "bombilla apagada";
});

botonEliminar.addEventListener("click", (e) => {
    document.querySelector("img").style.display = 'none';
});


const cajas = document.querySelectorAll("div");

// for (let [indice, caja] of cajas.entries()) {
//     // caja es el div sobre el que quiero colocar el onclick
//     // indice es la posicion de la caja dentro del array NodeList
//     caja.addEventListener("click", function (e) {
//         e.target.style.backgroundImage = "url(img/" + (indice + 1) + ".jpg)";
//         e.target.innerHTML = "";
//     });


// }

// realizarlo con foreach
cajas.forEach(function (caja, indice) {
    // caja es el div sobre el que quiero colocar el onclick
    // indice es la posicion de la caja dentro del array NodeList
    caja.addEventListener("click", function (e) {
        e.target.style.backgroundImage = "url(img/" + (indice + 1) + ".jpg)";
        e.target.innerHTML = "";
    });
});
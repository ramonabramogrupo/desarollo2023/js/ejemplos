// variables y constantes

const vector = ["Lunes", "martes", "miercoles", "jueves", "viernes"];

// recorrer el array y mostrarle en pantalla

// utilizando for
document.write("<br>Mostrando con for<br>");
for (let c = 0; c < vector.length; c++) {
    document.write(vector[c] + " ");
}

// utilizando foreach
document.write("<br>Mostrando con foreach<br>");
vector.forEach(function (valor) {
    document.write(valor + " ");
});

// utilizando for of
document.write("<br>Mostrando con for of<br>");
for (let valor of vector) {
    document.write(valor + " ");
}

// utilizando for of (leyendo indice y valor)
document.write("<br>Mostrando con for of con entries<br>");
for (let [indice, valor] of vector.entries()) {
    document.write(valor + " ");
}

// utilizando for in
document.write("<br>Mostrando con for in<br>");
for (let indice in vector) {
    document.write(vector[indice] + " ");
}








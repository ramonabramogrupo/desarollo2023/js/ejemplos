// coloco un onclick a cada li
// variables y constantes
const li = document.querySelectorAll('ul>li');

// colocar un escuchador a cada li
// cuando pulse en el li lo coloque rojo

li.forEach(function (valor) {
    valor.addEventListener("click", function (evento) {
        evento.target.style.backgroundColor = "red";
    });
});

// // coloco un click al ul
// const ul = document.querySelector('ul');

// ul.addEventListener("click", function (evento) {
//     evento.target.style.backgroundColor = "red";
// });
let a = [1, 2, 3, 4, 12, 34];

console.log('for of');
for (let valor of a) {
    // valor
    console.log(valor);
}
console.log('for of con entries');
for (let [indice, valor] of a.entries()) {
    //indice
    console.log(indice);

    //valor
    console.log(valor);
}

console.log('foreach');
a.forEach(function (valor, indice) {
    // valor
    console.log(valor);

    //indice
    console.log(indice);
});

console.log('for in');
for (let indice in a) {
    console.log(a[indice]);
}

console.log('for');
for (let c = 0; c < a.length; c++) {
    console.log(a[c]);
}


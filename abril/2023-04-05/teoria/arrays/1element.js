// creando un array de elementos
// NodeList

const li = document.querySelectorAll('li');


// recorrer el NodeList
console.log("mostrando con for");
for (let c = 0; c < li.length; c++) {
    console.log(li[c].innerHTML);
}

// recorrer con for of
console.log("mostrando con for of");
for (let valor of li) {
    console.log(valor.innerHTML);
}

// recorrer con foreach
console.log("mostrando con foreach");
li.forEach(function (valor) {
    console.log(valor.innerHTML);
});

// recorrer con for in
console.log("mostrando con for in");
for (let indice in li) {
    console.log(li[indice].innerHTML);
}
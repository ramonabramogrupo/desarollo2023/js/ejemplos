/**
 * la funcion recibe un string
 * la funcion me cuenta cuantas 
 * veces se repite cada vocal
 * me retorna un objeto de este tipo
 * {
 * a:0,
 * e:0,
 * i:0,
 * o:0,
 * u:0
 * }
 */

function contar(texto) {
    let vocales = {
        a: 0,
        e: 0,
        i: 0,
        o: 0,
        u: 0
    }
    texto = texto.toLowerCase()
    texto = texto.split("")

    for (let c = 0; c < texto.length; c++) {
        //for(let [c,letra] of texto.entries()){
        //for (let c in texto) {
        /*if (
            texto[c] == 'a' ||
            texto[c] == 'e' ||
            texto[c] == 'i' ||
            texto[c] == 'o' ||
            texto[c] == 'u'
        ) {
            vocales[texto[c]]++;
        }*/

        switch (texto[c]) {
            case 'a':
                vocales.a++;
                break;
            case 'e':
                vocales.e++;
                break;
            case 'i':
                vocales.i++;
                break;
            case 'o':
                vocales.o++;
                break;
            case 'u':
                vocales.u++;
                break;
        }

    }



    return vocales;

}

console.log(contar("Ejemplo de clase"));
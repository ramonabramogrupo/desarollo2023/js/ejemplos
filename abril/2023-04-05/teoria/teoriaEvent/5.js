let lista = document.querySelector('ul');

lista.addEventListener("click", function(e) {
    // objeto sobre el que se pulsa clic
    console.log(e.target);

    // escuchador (ul)
    console.log(this);
    console.log(lista);
    console.log(e.currentTarget);
})
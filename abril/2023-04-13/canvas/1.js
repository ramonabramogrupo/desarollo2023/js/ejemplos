// constante que apunta al lienzo
const lienzo = document.querySelector('#canvas');

// constante para dibujar en el lienzo
const contexto = lienzo.getContext("2d");

// dibujar un rectangulo relleno
contexto.fillRect(
    25, // x
    5, // y
    300, // ancho
    100 // alto
);

// dibujar un rectangulo sin relleno
contexto.strokeRect(
    25, // x
    110, // y
    300, //ancho
    100 //alto
)

// cambiar el color con el que rellena
contexto.fillStyle = "rgb(200,0,0)";

// cambiar el color del borde
contexto.strokeStyle = "rgb(0,150,50)";

contexto.fillRect(
    5, //x
    5, //y
    200, //ancho
    100 //alto
);



// dibujar rectangulo sin relleno
contexto.strokeRect(
    400,
    400,
    50,
    50
);
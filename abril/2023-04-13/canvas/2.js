// constante que apunta al lienzo

const lienzo = document.querySelector('#canvas');

// constante que me permite dibujar en el lienzo en 2d
const contexto = lienzo.getContext("2d");

//  creo objeto rectangulo
const rectangulo = {
    x: 0,
    y: 0,
    ancho: 0,
    alto: 0,
    relleno: true,
    borde: "rgb(0,0,0)",
    fondo: "rgb(0,0,0)",
    dibujar: function (contexto) {
        if (this.relleno) {
            contexto.fillStyle = this.fondo;
            contexto.fillRect(
                this.x,
                this.y,
                this.ancho,
                this.alto
            );
        } else {
            contexto.strokeStyle = this.borde;
            contexto.strokeRect(
                this.x,
                this.y,
                this.ancho,
                this.alto
            );
        }
    },
}

// dibujar un rectangulo
rectangulo.x = 100;
rectangulo.y = 100;
rectangulo.ancho = 400;
rectangulo.alto = 200;
rectangulo.fondo = "rgb(0,200,200)";

rectangulo.dibujar(contexto);

// dibujar otro rectangulo
rectangulo.x = 550;
rectangulo.ancho = 50;
rectangulo.relleno = false;
rectangulo.borde = "green";
rectangulo.dibujar(contexto);
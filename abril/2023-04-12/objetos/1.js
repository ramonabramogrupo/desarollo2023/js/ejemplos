// cosntante que apunta al boton
const boton = document.querySelector('button');

// creo un array para almacenar a todas las personas
const personas = [];

// eventos
boton.addEventListener("click", (e) => {
    // creo un objeto
    const persona = {
        nombre: "",
        edad: 0,
    };

    // constantes que apunta a los inputs
    const inputNombre = document.querySelector('#nombre');
    const inputEdad = document.querySelector('#edad');

    persona.nombre = inputNombre.value;
    persona.edad = inputEdad.value;
    console.log(persona);

    // añadir al array personas la ultima persona
    personas.push(persona);

});
// constantes que apunta a cada boton
const boton = document.querySelector('#enviar');
const botonMostrar = document.querySelector('#mostrar');

// creo un array para almacenar a todas las personas
const personas = [];

// eventos
boton.addEventListener("click", (e) => {
    // creo un objeto
    const persona = {
        // propiedades
        nombre: "",
        apellidos: "",
        edad: 0,
        // metodos
        nombreCompleto: function () {
            return `${this.nombre} ${this.apellidos}`;
        },
        mostrar: function (nombre) {
            // constante que apunta a la caja donde quiero escribir
            const caja = document.querySelector(nombre);

            caja.innerHTML += this.nombreCompleto() + "<br>";
        },
        // metodo especial que ya tiene Object
        // este metodo se llama cuando intento tratar un objeto como un texto
        toString: function () {
            return this.nombreCompleto();
        },
    };

    // constantes que apunta a los inputs
    const inputNombre = document.querySelector('#nombre');
    const inputApellidos = document.querySelector('#apellidos');
    const inputEdad = document.querySelector('#edad');

    persona.nombre = inputNombre.value;
    persona.apellidos = inputApellidos.value;
    persona.edad = inputEdad.value;

    // añadir al array personas la ultima persona
    personas.push(persona);

    // me muestre el nombre completo en la consola
    console.log(persona.nombreCompleto());

    // llamar al metodo que me muestra la persona en la caja salida
    persona.mostrar("#salida");

});

botonMostrar.addEventListener("click", (e) => {
    //  constante que apunta al div donde quiero mostrar todas las personas
    const caja = document.querySelector('#salida');

    caja.innerHTML = personas;

})
// constante que apunta a la musica
const musica = document.querySelector('audio');

// constante que apunta a foto
const img = document.querySelector("#letras");

// creo un array con la precarga de todas las 
// imagenes que se muestran al pulsar las teclas
const imgArray = [];

// inicializo el array con las imagenes
// tecla a es el keycode 65
// tecla b es el keycode 66

// estoy creando un array de imagenes
for (i = 65; i < 91; i++) {
    imgArray[i] = new Image();
    imgArray[i].src = "imgs/" + i + ".gif";
}

// control eventos

// cuando pulso la tecla
document.addEventListener("keydown", (evento) => {
    // coger el codigo de la tecla pulsado
    let codigo = evento.keyCode;

    // constante que apunta a foto
    const img = document.querySelector("#letras");

    // constante que apunta a la musica
    const musica = document.querySelector('audio');

    // compruebo que la tecla este entre la letra a y la letra z
    if (codigo > 64 && codigo < 91) {
        img.src = imgArray[codigo].src;
    }

    // quiero que la musica se pare
    musica.pause();
});

// cuando suelto la tecla
document.addEventListener("keyup", (evento) => {
    // constante que apunta a foto
    const img = document.querySelector("#letras");

    // constante que apunta a la musica
    const musica = document.querySelector('audio');

    // coloco la imagen de todas las bombillas apagadas
    img.src = "imgs/vacio.gif";

    //musica.currentTime+=10;
    //musica.playbackRate += .1;
    musica.play();
});
